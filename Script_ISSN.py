#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  3 13:06:29 2018

@author: wences
"""
        
import json
import requests

import sys

# LECTURA DE LOS IDS

with open('details.txt', 'r') as file:
    ids = []
    for id in file:
        id = id.rstrip()
        ids.append(id)

# Consulta y generacion de ISSN
outfp1 = open('resultados1.csv', "w")
outfp2 = open('resultados2.csv', "w")
errores = open('errores.csv', 'w')

contador = 0

error_issn = 0
error_api = 0

for id in ids:
    
    contador = contador + 1
    #print(contador)
    
    url = 'http://api.altmetric.com/v1/id/'+str(id)
    try: # Por si no tiene recurso
        response = requests.get(url)
        result = json.loads(response.text)
        try: # Por si no tiene un ISSN
            lista = [] # Como puede incluir más de un ISSN creo una lista para cada consulta
            issns = result['issns']
            
            if(issns != []): # Para evitar aquellos con el campo vacio
                lista.append('https://www.altmetric.com/details/'+str(id)) # Añade la URL para enlazar despues
                for issn in issns:
                    outfp1.write(str(id) + ',' + issn + '\n') # Se escibe la instancia ID+ISSN
                    lista.append(issn) # Se van añadiendo los diferentes ISSN detectados
                outfp2.write(','.join(lista) + '\n') # Se escibe la instancia ID+ISSNs

        except:
            if(str(sys.exc_info()[0]) == '<class \'KeyError\'>'):
                errores.write(str(id) + ',No encontrado ISSN' + '\n')
                error_issn = error_issn + 1
    except:
        print('Unexpected error:', sys.exc_info()[0])
        errores.write(str(id) + ',Error API' + '\n')
        error_api = error_api + 1

outfp1.close()
outfp2.close()
errores.close()

print('Exportación realizada: ' + str(error_issn) + ' ISSN erróneos y ' + str(error_api) + ' errores con la API')
