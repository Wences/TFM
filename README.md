# La Ciencia a través de Wikipedia: una nueva perspectiva
### Máster Oficial en Ciencia de Datos e Ingeniaría de Computadores (UGR)
Trabajo de Fin de Máster

Convocatoria de junio

Curso 2017/2018

**Autor**: Wenceslao Arroyo Machado

**Director**: Enirque Herrera Viedma
## Scripts
En este repositorio se incluyen los nueve scripts en R y el script en Python empleados para el desarrollo del TFM, todos ellos con comentarios detallando los procesos llevados a cabo.