# Directorio del script
setwd(dirname(rstudioapi::getSourceEditorContext()$path))

# Bibliotecas necesarias
require(VIM)
require(Hmisc)
require(data.table)

#####################################
# Union de los 2 conjuntos de datos #
#####################################

# Parte 1
altmetricParte1 <- read.csv2('Altmetric/Altmetric_1.csv',
                             header = TRUE,
                             stringsAsFactors = FALSE,
                             quote = '"',
                             sep = ',')

# Parte 2
altmetricParte2 <- read.csv2('Altmetric/Altmetric_2.csv',
                             header = TRUE,
                             stringsAsFactors = FALSE,
                             quote = '"',
                             sep = ',')

# Union
altmetricTotal <- rbind(altmetricParte1, altmetricParte2)

# Analisis del conjunto resultante
dim(altmetricTotal)[1] # 1.433.457 referencias
length(unique(altmetricTotal[,'Details.Page.URL'])) # 960.017 trabajos referenciados
length(unique(altmetricTotal[,'Mention.Title'])) # 321.470 articulos de Wikipedia
min(altmetricTotal$Mention.Date) # 2004-10-15
max(altmetricTotal$Mention.Date) # 2018-04-10

################################
# Analisis de valores perdidos #
################################

# Copia del original para realizar modificaciones
altmetricPerdidos <- altmetricTotal

# Conversion de nulos en strings en NAs
altmetricPerdidos[which(altmetricPerdidos[,'Research.Output.Title']==''),'Research.Output.Title'] <- NA
altmetricPerdidos[which(altmetricPerdidos[,'Journal.Collection.Title']==''),'Journal.Collection.Title'] <- NA
altmetricPerdidos[which(altmetricPerdidos[,'Output.Type']==''),'Output.Type'] <- NA
altmetricPerdidos[which(altmetricPerdidos[,'Publication.Date']==''),'Publication.Date'] <- NA
altmetricPerdidos[which(altmetricPerdidos[,'Details.Page.URL']=='https://www.altmetric.com/details/'),'Details.Page.URL'] <- NA
altmetricPerdidos[which(altmetricPerdidos[,'DOI']==''),'DOI'] <- NA
altmetricPerdidos[which(altmetricPerdidos[,'National.Clinical.Trial.ID']==''),'National.Clinical.Trial.ID'] <- NA
altmetricPerdidos[which(altmetricPerdidos[,'URI']==''),'URI'] <- NA
altmetricPerdidos[which(altmetricPerdidos[,'PubMedCentral.ID']==''),'PubMedCentral.ID'] <- NA
altmetricPerdidos[which(altmetricPerdidos[,'Handle.net.IDs']==''),'Handle.net.IDs'] <- NA
altmetricPerdidos[which(altmetricPerdidos[,'ADS.Bibcode']==''),'ADS.Bibcode'] <- NA
altmetricPerdidos[which(altmetricPerdidos[,'arXiv.ID']==''),'arXiv.ID'] <- NA
altmetricPerdidos[which(altmetricPerdidos[,'RePEc.ID']==''),'RePEc.ID'] <- NA
altmetricPerdidos[which(altmetricPerdidos[,'URN']==''),'URN'] <- NA

# Cambiar los puntos de los nombres de las columnas por espacios
names(altmetricPerdidos) <- sub('\\.',' ',names(altmetricPerdidos))
names(altmetricPerdidos) <- sub('\\.',' ',names(altmetricPerdidos))
names(altmetricPerdidos) <- sub('\\.',' ',names(altmetricPerdidos))

#png('Imagenes/Valores_perdidos_original.png', units="in", width=10, height=6.5, res=300)
VIM::aggr(altmetricPerdidos, labels=names(altmetricPerdidos), col=c("dodgerblue","firebrick1"),  varheight = FALSE, cex.axis=.4, gap=1, sortVars=TRUE, ylab=c('Proporcion de valores perdidos','Instancias'))
#dev.off()

#################################
# Preprocesamiento de los datos #
#################################

# Eliminar instancias con perdidos en Details.Page.URL
detailsNulos <- which(altmetricTotal[,'Details.Page.URL'] == 'https://www.altmetric.com/details/')
# Research.Output.Title y DOI comparten perdidos
Hmisc::describe(altmetricTotal[detailsNulos,])
altmetric <- altmetricTotal[-detailsNulos,]

# Eliminar instancias con perdidos en Research.Output.Title
researchNulos <- which(altmetric[,'Research.Output.Title'] == '')
altmetric <- altmetric[-researchNulos,]

# Eliminar instancias con perdidos en Publication.Date
datosPerdidos <- which(altmetric[,'Publication.Date'] == '')
altmetric <- altmetric[-datosPerdidos,]

#####################
# Errores en el DOI #
#####################

# 340 DOI problematicos
altmetric[which(!grepl('(^10.[0-9].*)|(^$)', altmetric[, 'DOI'], perl = TRUE)),'DOI']

# Transformaciones automatizadas para arreglarlos
altmetric[which(grepl('(^doi:)', altmetric[, 'DOI'], perl = TRUE)),'DOI'] <- substr(altmetric[which(grepl('(^doi:)', altmetric[, 'DOI'], perl = TRUE)),'DOI'],5,100)
altmetric[which(grepl('(^http://1)', altmetric[, 'DOI'], perl = TRUE)),'DOI'] <- substr(altmetric[which(grepl('(^http://1)', altmetric[, 'DOI'], perl = TRUE)),'DOI'],8,100)
altmetric[which(grepl('(^http://dx.doi.org/)', altmetric[, 'DOI'], perl = TRUE)),'DOI'] <- substr(altmetric[which(grepl('(^http://dx.doi.org/)', altmetric[, 'DOI'], perl = TRUE)),'DOI'],19,100)

# Transformaciones manuales para arreglarlos
altmetric[which(!grepl('(^10.[0-9].*)|(^$)', altmetric[, 'DOI'], perl = TRUE)),'DOI'][305] <- '10.1038/nrm2008'
altmetric[which(!grepl('(^10.[0-9].*)|(^$)', altmetric[, 'DOI'], perl = TRUE)),'DOI'] <- ''


##########
# Idioma #
##########

# Deteccion de idiomas mediante Mention.URL
language <- substr(altmetric[,'Mention.URL'],8,9)
100*(table(language)/length(language))

# Seleccion de ingles
indexLanguage <- which(language=='en')
altmetric <- altmetric[indexLanguage,]

##########################
# Referencias duplicadas #
##########################

# Creacion de variables auxiliar (altmetric2) para las fechas
fechas <- strptime(altmetric[,'Mention.Date'], format='%Y-%m-%d %H:%M:%S')
index <- 1:dim(altmetric)[1]
altmetric2 <- cbind(altmetric[,c('Mention.Date','Mention.Title','Research.Output.Title')], fechas, index)

# Verificacion
sum(as.character(altmetric2[,1])==as.character(altmetric2[,4])) == dim(altmetric2)[1]

# Transformacion a data.table
altmetric2 <- data.table::as.data.table(altmetric2) 
dim(unique(altmetric2[,c('Mention.Title','Research.Output.Title')]))[1] # 1.211.904 referencias

# Agrupamiento de las referencias repetidas por la maxima Mention.Date
altmetric2 <- altmetric2[altmetric2[, .I[which.max(fechas)], by=list(Mention.Title,Research.Output.Title)]$V1]

# Seleccion de las referencias correctas
altmetricFinal  <- altmetric[altmetric2$index,]

# Verificacion
all(altmetricFinal$Mention.Title == altmetric2$Mention.Title) # TRUE
all(altmetricFinal$Mention.Date == altmetric2$Mention.Date) # TRUE

dim(altmetricFinal)[1] # 1.211.904 referencias
length(unique(altmetricFinal$Mention.Title)) # 288.290 articulos de Wikipedia
length(unique(altmetricFinal$Details.Page.URL)) # 857.087 trabajos referenciados

# Seleccion de las variables necesarias
altmetricFinal2 <- altmetricFinal[,c('Mention.Date', 'Mention.Title', 
                                     'Research.Output.Title', 'Output.Type', 
                                     'Publication.Date', 'Altmetric.Attention.Score', 
                                     'Details.Page.URL', 'DOI')]

################################
# Analisis de valores perdidos #
################################

# Repeticion del mismo proceso que al principio
altmetricPerdidos <- altmetricFinal2

altmetricPerdidos[which(altmetricPerdidos[,'Research.Output.Title']==''),'Research.Output.Title'] <- NA
altmetricPerdidos[which(altmetricPerdidos[,'Output.Type']==''),'Output.Type'] <- NA
altmetricPerdidos[which(altmetricPerdidos[,'Publication.Date']==''),'Publication.Date'] <- NA
altmetricPerdidos[which(altmetricPerdidos[,'Details.Page.URL']=='https://www.altmetric.com/details/'),'Details.Page.URL'] <- NA
altmetricPerdidos[which(altmetricPerdidos[,'DOI']==''),'DOI'] <- NA

names(altmetricPerdidos) <- sub('\\.',' ',names(altmetricPerdidos))
names(altmetricPerdidos) <- sub('\\.',' ',names(altmetricPerdidos))
names(altmetricPerdidos) <- sub('\\.',' ',names(altmetricPerdidos))

#png('Imagenes/Valores_perdidos_limpio.png', units="in", width=10, height=6.5, res=300)
aggr(altmetricPerdidos, labels=names(altmetricPerdidos), col=c("dodgerblue","firebrick1"),  varheight = FALSE, cex.axis=.4, gap=1, sortVars=TRUE, ylab=c('Proporcion de valores perdidos','Instancias'))
#dev.off()

###############
# Exportacion #
###############

write.csv2(altmetricFinal2, 'Altmetric/Altmetric_clean.csv',
           row.names = FALSE)
